#include "commandInterface.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <iostream>
#include <dlfcn.h>

std::vector<std::string> getDllNames(std::string dir) {
    std::vector<std::string> names;
    DIR *dp;
    struct dirent *dirp;
    if((dp = opendir(dir.c_str())) == NULL)
        throw std::runtime_error("commands directory not found");
    while ((dirp = readdir(dp)) != NULL) {
        std::string fileName = std::string(dirp->d_name);
        if(fileName.find(".so") != std::string::npos)
            names.push_back(fileName);
    }
    closedir(dp);
    return names;
}

std::pair<void*, std::shared_ptr<command>> commandInterface::getCommandPtr(std::string filename) {
    void* hndl = dlopen(filename.c_str(), RTLD_NOW);
    if(hndl == NULL)
        throw std::runtime_error(dlerror());
    command* (*create)();
    create = (command* (*)())dlsym(hndl, "create_object");
    std::shared_ptr<command> _command(create()); 
    return std::make_pair(hndl, _command);
}

void commandInterface::loadCommands() {
    std::vector<std::string> files;
    try {
        files = getDllNames(dir.c_str() );
    } catch (const std::runtime_error& e) {
        std::cout << "error loading commands dir." << std::endl << e.what() << std::endl;
    }
    for(std::string i : files)
        loadCommand(i);
}

void commandInterface::loadCommand(std::string input, bool reload) {
    std::pair<void*, std::shared_ptr<command>> cmd_ptr;
    try {
        cmd_ptr = getCommandPtr("./" + dir + "/" + input);
    } catch(const std::runtime_error& e) {
        std::cout << e.what() << std::endl;
        return;
    }
    cmd_ptr.second -> init();
    std::string name = cmd_ptr.second -> getInfo().name;
    commandMap.insert(std::make_pair(name, cmd_ptr));
    std::cout << "successfully loaded" + input << std::endl;
}

void commandInterface::reloadCommands() {
    unloadAll();
    loadCommands();
}

void commandInterface::unloadAll() {
    for(std::string i : getCommandNames())
        unloadCommand(i);
    commandMap.clear();
}

void commandInterface::unloadCommand(std::string input) {
    auto x = commandMap.find(input);
    if(x != commandMap.end()) {
        void* ptr = x -> second.first;
        commandMap.erase(input);
        dlclose(ptr);
    }
}

bool commandInterface::checkCommandExistance(std::string input) {
    if(commandMap.find(input) != commandMap.end())
        return true;
    return false;
}

std::vector<std::string> commandInterface::getCommandNames() {
    std::vector<std::string> tmp;
    for(auto i : commandMap)
        tmp.push_back(i.second.second -> getInfo().name);
    return tmp;
}

std::shared_ptr<command> commandInterface::getCommand(std::string input) {
    auto x = commandMap.find(input);
    if(x != commandMap.end())
        return x -> second.second;
    else 
        throw std::out_of_range("command not found");
}

void commandInterface::runCommand(std::string input) {
    getCommand(input) -> run();
}

void commandInterface::shutdown() {
    unloadAll();
}