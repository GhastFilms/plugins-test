#ifndef CMD3_H
#define CMD3_H

#include "../../command.h"
#include <string>
#include <iostream>

class e : public command {
private:
    commandInfo info;
public:
    void run();
    void init();
    commandInfo getInfo();
};

#endif