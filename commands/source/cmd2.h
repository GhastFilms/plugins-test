#ifndef CMD2_H
#define CMD2_H

#include "../../command.h"
#include <string>
#include <iostream>

class joke : public command {
private:
    commandInfo info;
public:
    void run();
    void init();
    commandInfo getInfo();
};

#endif