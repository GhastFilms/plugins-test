#include <string>
#include <iostream>
#include "cmd2.h"

extern "C" joke* create_object() {
  return new joke;
}

void joke::run() {
    std::cout << "haha this is funny joke" << std::endl;
}

void joke::init() {
    info.name = "joke";
}

commandInfo joke::getInfo() {
    return info;
}