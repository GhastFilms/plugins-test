#ifndef CMD1_H
#define CMD1_H

#include "../../command.h"
#include <string>
#include <iostream>

class meme : public command {
private:
    commandInfo info;
public:
    void run();
    void init();
    commandInfo getInfo();
};

#endif