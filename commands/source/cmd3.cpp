#include <string>
#include <iostream>
#include "cmd3.h"

extern "C" e* create_object() {
  return new e;
}

void e::run() {
    std::cout << "when you E" << std::endl;
}

void e::init() {
    info.name = "e";
}

commandInfo e::getInfo() {
    return info;
}