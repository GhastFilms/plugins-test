#include <string>
#include <iostream>
#include "cmd1.h"

extern "C" meme* create_object() {
  return new meme;
}

void meme::run() {
    std::cout << "https://www.reddit.com/r/memes/comments/9xp2mo/he_is_a_real_hero/" << std::endl;
}

void meme::init() {
    info.name = "meme";
}

commandInfo meme::getInfo() {
    return info;
}