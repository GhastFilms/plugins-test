#ifndef COMMAND_H
#define COMMAND_H
#include <string>

struct commandInfo {
     std::string name;
};

class command {
private:
    commandInfo info;
public:
    command() {};
    virtual ~command() {};
    virtual void init() {};
    virtual void run() {};
    virtual commandInfo getInfo() {return info; }
    
};

#endif