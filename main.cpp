#include "command.h"
#include "commandInterface.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include <functional>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <iostream>
#include <cstring>


std::vector<std::string> split(std::string str2, std::string sep2) {
    char* cstr2=const_cast<char*>(str2.c_str());
    char* current1;
    std::vector<std::string> arr2;
    current1=std::strtok(cstr2,sep2.c_str());
    while(current1!=NULL){
        arr2.push_back(current1);
        current1=std::strtok(NULL,sep2.c_str());
    }
    return arr2;
}

void cli(int argc, std::vector<std::string> argv, std::string input, commandInterface& cmds) {
    if(argc != 0) {
        //tries to call commandInterface::runCommand() 
        if(argv.at(0) == "run") {
            try{
                cmds.runCommand(argv.at(1));
            } catch(const std::out_of_range& e) {
                std::cout << "unknown plugin use \"commands\" to get a list of plugins" << std::endl;
            }
        }
        //prints all commands
        else if(argv.at(0) == "commands") {
            for(std::string i : cmds.getCommandNames()) 
                std::cout << " - " + i << std::endl;
        }
        //help message
        else if(argv.at(0) == "help") {
            std::cout << "run " << std::endl;
            std::cout << " - run (command): run the inputed command"              << std::endl; 
            std::cout << " - commands: gets a list of runable commands"           << std::endl; 
            std::cout << " - reload: reloads a command"                           << std::endl; 
            std::cout << " - unload (command): unloads a command from the system" << std::endl; 
            std::cout << " - reload: checks the commands directory and "          << std::endl; 
            std::cout << " - exit: exits the program"                             << std::endl; 
        }
        else if(argv.at(0) == "reload") cmds.reloadCommands();
        else if(argv.at(0) == "unload") cmds.unloadCommand(argv.at(1));
        else if(argv.at(0) == "exit") { cmds.shutdown(); exit(0); } 
        else                            std::cout << "unknown command use \"help\" to get a list of commands " << std::endl;
    }
}

int main() {
    std::cout << "---plugins test---"     << std::endl;
    std::cout << "enter \"exit\" to quit" << std::endl; 
    commandInterface commands = commandInterface();
    //cli loop
    std::string input;
    while(true) {
        std::cout << "$ ";
        if   (!std::getline(std::cin, input)) {}
        else cli(split(input, " ").size(), split(input, " "), input, commands);   
    }
}