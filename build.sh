g++ main.cpp commandInterface.cpp --std=c++11 -ldl -o plugins
cd commands/source
g++ -fPIC -shared cmd1.cpp -o cmd1.so
g++ -fPIC -shared cmd2.cpp -o cmd2.so
g++ -fPIC -shared cmd3.cpp -o cmd3.so
mv cmd1.so ..
mv cmd2.so ..
mv cmd3.so ..
