#ifndef COMMANDINTERFACE_H
#define COMMANDINTERFACE_H

#include "command.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>


class commandInterface {
    private:
        std::unordered_map<std::string, std::pair<void*, std::shared_ptr<command>>> commandMap;
        std::pair<void*, std::shared_ptr<command>>  getCommandPtr(std::string);
        std::string dir = "commands";
    public:
        //constructor and destructor
        commandInterface (std::string _dir                  ): dir(_dir) { loadCommands();          };
        commandInterface (bool load = true                  )            { if(load) loadCommands(); };
        commandInterface (std::string _dir, bool load = true): dir(_dir) { if(load) loadCommands(); };
        ~commandInterface() {};
        // command management
        void loadCommands  ();
        void reloadCommands();
        void loadCommand   (std::string, bool = false);
        void unloadCommand (std::string);
        void unloadAll     ();
        //command checking
        bool checkCommandExistance(std::string);
        std::vector<std::string> getCommandNames();
        //command interaction
        std::shared_ptr<command> getCommand(std::string); // this is being annoying so i will get it working later
        void runCommand(std::string);
        void shutdown();
};

#endif